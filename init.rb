# -*- coding: utf-8 -*-
require 'redmine'
require 'site_feedback_application_hooks'
require 'settings_controller_patch'

Redmine::Plugin.register :redmine_site_feedback do
  name 'Redmine Site Feedback plugin'
  author 'Akiko Takano'
  author_url 'http://twitter.com/akiko_pusu'  
  description 'Plugin to provide site-wide "feedback tab", that allows users to access feedback forums, issue tracker & information right away.
'
  version '0.0.2'
  requires_redmine :version_or_higher => '2.0.0'
  url 'https://bitbucket.org/akiko_pusu/redmine_site_feedback'

  settings :partial => 'settings/redmine_site_feedback',
    :default => {
      'enable' => 'false',
      'hide_anonymous' => 'false',
      'hide_issue_show_page' => 'false',
      'feedback_url' => '',
      'feedback_description' => 'This description is shown as tooltip, title attribute, when mouse is over the feedback tab.'
    }
    
  Rails.configuration.to_prepare do
    unless SettingsController.included_modules.include?(SettingsControllerPatch)   
      SettingsController.send(:include, SettingsControllerPatch)
    end  
  end
    
end
