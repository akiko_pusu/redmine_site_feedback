(function(){
	jQuery.fn.feedback = function(options){
		
		var defaults = {
			'mouseoverColor': '#ff0000',
			'mouseoffColor': '#222222',
			'position': 'right'
		}
		
		return this.each(function(){
			
			//defaults  object extends options object
			options = jQuery.extend(defaults, options);
			
			var obj = jQuery(this);
			var theWindow = jQuery(window);
			
			function hoverOn() {obj.css('background-color', options.mouseoverColor);}
			function hoverOff() {obj.css('background-color', options.mouseoffColor);}
			
			obj.bind('mouseover', function(){hoverOn();});
			obj.bind('mouseout', function(){hoverOff();});
			obj.css('background-color', options.mouseoffColor); //set the intial background color on page load
			
			//provided image dimensions: image width=30, height=100
			function positionElement(){
				var elementWidth = obj.outerWidth();
				var elementHeight = obj.outerHeight();
				var windowWidth = theWindow.width();
				var windowHeight = theWindow.height();	
				
				if(options.position == "right"){
					var X2 = windowWidth - elementWidth - 2;
				}
				else if(options.position == "left"){
					var X2 = 0;
				}			
				
				//var Y2 = windowHeight/2 - elementHeight/2;
				var Y2 = windowHeight - elementHeight;
				
				X2 = X2 + "px";
				Y2 = Y2 + "px"; 
				obj.css({
					'left':X2,
					'top':Y2,
					'position':'fixed'
				});						
			} //end of positionElement function
			
			positionElement(); //position the element on page load
			
			//bind various event handlers to mouseover, mouseoff, and window resize events
			theWindow.bind('resize',function(){
				positionElement();
			});
									
		}); //end of return this.each
	};
})(jQuery);
