# Patch for banner plugin. This affects in "plugin" action of Redmine Settings 
# controller.
# Now banner plugin does not have own model(table). So, datetime informations 
# are stored as string and required datetime validation by controller.
# 
# TODO Store banner settings to banner's own model (table).   
#
require 'uri' 

module SettingsControllerPatch
  unloadable
  def self.included(base)
    base.send(:include, ClassMethods)
    base.class_eval do
      alias_method_chain(:plugin, :feedback_url_validation)
    end
  end
  module ClassMethods
    #
    # Before posting start / end date, do validation check.(In case setting "Use timer".) 
    #

    def plugin_with_feedback_url_validation
      return plugin_without_feedback_url_validation unless params[:id] == "redmine_site_feedback"      
      @plugin = Redmine::Plugin.find(params[:id])

      @partial = @plugin.settings[:partial]
      @settings = Setting.send "plugin_#{@plugin.id}"
      
      if request.post?
        feedback_url = "#{params[:settings][:feedback_url]}" 
        enable = "#{params[:settings][:enable]}"
        begin
          uri = URI.parse(feedback_url)
          if enable == "true"
            if !uri.kind_of?(URI::HTTP)
              flash[:error] = l(:error_bad_url_format)
              redirect_to :action => 'plugin', :id => @plugin.id
              return
            end
          end
        rescue => ex
          flash[:error] = l(:error_bad_url_format)
          redirect_to :action => 'plugin', :id => @plugin.id
          return        
        end     
      end
      # Continue to do default action
      plugin_without_feedback_url_validation
    end
  end
end
