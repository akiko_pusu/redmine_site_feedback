# -*- coding: utf-8 -*-
class SiteFeedbackApplicationHooks < Redmine::Hook::ViewListener
  def view_layouts_base_body_bottom(context = { })
    settings = Setting.send "plugin_redmine_site_feedback"
    return '' unless SiteFeedback.enable_feedback? && SiteFeedback.activate_tab?(User.current) 
     
    if context[:controller].class.name == 'IssuesController' and \
      context[:controller].action_name != 'index' and \
      settings['hide_issue_show_page'] == "true"
      return ''
    end
    
    # show feedback tab icon
    title = settings['feedback_description']
    img_src = image_tag('feedback.png', :plugin => 'redmine_site_feedback', :title => title.force_encoding('utf-8'),
      :width => 100, :height => 20, :alt => 'feedback', :id => 'feedback')    
    return img_src
  end
  
  def view_layouts_base_html_head(context = { })
    settings = Setting.send "plugin_redmine_site_feedback"
    return '' unless SiteFeedback.enable_feedback? && SiteFeedback.activate_tab?(User.current)
    
    if context[:controller].class.name == 'IssuesController' and \
      context[:controller].action_name != 'index' and \
     settings['hide_issue_show_page'] == "true"
      return ''
    end
         
    context[:controller].send(
      :render_to_string,
      {
        :partial => 'site_feedback/html_header'
      }
    )   
  end
end
