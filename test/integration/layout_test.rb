require File.expand_path(File.dirname(__FILE__) + '/../test_helper')

class LayoutTest < ActionController::IntegrationTest
  fixtures :projects, :trackers, :issue_statuses, :issues,
           :enumerations, :users, :issue_categories,
           :projects_trackers,
           :roles,
           :member_roles,
           :members,
           :enabled_modules,
           :workflows

  def setup
    # plugin setting
    @settings = Setting.send "plugin_redmine_site_feedback"
    @settings["enable"] = "true"
    @settings["feedback_url"] = "http://redmine.org/"
    @settings["hide_anonymous"] = "true"    
  end

  def test_feedback_tab_not_visible_when_login_required_with_anonymous
    @settings["hide_anonymous"] = "true"
    get '/'
    assert_no_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end

  def test_feedback_tab_visible_with_anonymous
    @settings["hide_anonymous"] = "false"
    get '/'
    assert_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end
  
  def test_feedback_tab_visible_when_login_required_with_authenticated
    @settings["hide_anonymous"] = "true" 
    log_user('jsmith', 'jsmith')
    get '/'
    assert_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end

  def test_feedback_tab_visible_when_login_required_with_authenticated
    @settings["hide_anonymous"] = "false" 
    log_user('jsmith', 'jsmith')
    get '/'
    assert_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end  
  
  def test_feedback_tab_visible_when_issue_index_page
    @settings["hide_issue_show_page"] = "true" 
    log_user('jsmith', 'jsmith')
    get '/projects/ecookbook/issues'
    assert_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end  
  
  def test_feedback_tab_not_visible_when_issue_show_page
    @settings["hide_issue_show_page"] = "true" 
    log_user('jsmith', 'jsmith')
    get '/issues/1'
    assert_no_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end
  
  def test_feedback_tab_visible_when_issue_show_page
    @settings["hide_issue_show_page"] = "false"
    log_user('jsmith', 'jsmith')
    get '/issues/1'
    assert_tag :script,
      :attributes => {:src => %r{feedback.jQuery.js}},
      :parent => {:tag => 'head'}
  end     
end
