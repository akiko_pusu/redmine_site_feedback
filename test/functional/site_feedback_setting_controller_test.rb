require File.expand_path(File.dirname(__FILE__) + '/../test_helper')

class SiteFeedbackSettingControllerTest < ActionController::TestCase
  fixtures :users
  include Redmine::I18n
  def setup
    @controller = SettingsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current = nil
    @request.session[:user_id] = 1 # admin
    
    # plugin setting
    @plugin = Redmine::Plugin.find('redmine_site_feedback')
    @partial = @plugin.settings[:partial]
    @settings = Setting["plugin_#{@plugin.id}"]    
  end
  
  def test_get_site_feedback_settings
    get :plugin, :id => "redmine_site_feedback"   
    assert_response :success 
    assert_template 'settings/plugin'
    assert_select 'h2', /Redmine Site Feedback plugin/, "#{@response.body}"
  end
  
  def test_post_site_feedback_settings_with_bad_format
    # set bad format (May happen URI::InvalidURIError)
    post :plugin, :id => "redmine_site_feedback",
      :settings => {:enable => "true", :feedback_url => "test url", :hide_anonymous => "false", 
      :hide_issue_show_page => "false",
      :feedback_description => "This description is shown as tooltip, title attribute, when mouse is over the feedback tab."}
    assert_response :redirect
    assert_redirected_to :controller => 'settings', :action => 'plugin', :id => "redmine_site_feedback"
    assert_equal I18n.t(:error_bad_url_format), flash[:error] 
  end

  def test_post_site_feedback_settings_with_good_format
    # set good format
    post :plugin, :id => "redmine_site_feedback",
      :settings => {:enable => "true", :feedback_url => "http://redmine.org/", :hide_anonymous => "false", 
      :hide_issue_show_page => "false",
      :feedback_description => "This description is shown as tooltip, title attribute, when mouse is over the feedback tab."}
    assert_response :redirect
    assert_redirected_to :controller => 'settings', :action => 'plugin', :id => "redmine_site_feedback"
    assert_equal I18n.t(:notice_successful_update), flash[:notice]
  end
  
  def test_post_site_feedback_settings_with_bad_protocol
    # set good format
    post :plugin, :id => "redmine_site_feedback",
      :settings => {:enable => "true", :feedback_url => "file:///redmine.org/", :hide_anonymous => "false", 
      :hide_issue_show_page => "false",
      :feedback_description => "This description is shown as tooltip, title attribute, when mouse is over the feedback tab."}
    assert_response :redirect
    assert_redirected_to :controller => 'settings', :action => 'plugin', :id => "redmine_site_feedback"
    assert_equal I18n.t(:error_bad_url_format), flash[:error] 
  end   
  
end