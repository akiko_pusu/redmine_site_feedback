require File.expand_path(File.dirname(__FILE__) + '/../test_helper')
require File.expand_path(File.dirname(__FILE__) + '/../../lib/site_feedback_application_hooks')
class SiteFeedbackApplicationHooksTest < ActiveSupport::TestCase
  def setup
    @target = SiteFeedbackApplicationHooks.instance
    @context = {}  
  end
  
  test "should hidden feecback_tab" do
    Setting.plugin_redmine_site_feedback['enable'] = "false"
    res = @target.view_layouts_base_body_bottom(@context)
    assert_equal true, res.blank?   
  end
  
  test "should display feecback_tab" do
    Setting.plugin_redmine_site_feedback['enable'] = "true"
    Setting.plugin_redmine_site_feedback['feedback_url'] = "http://redmine.org/"
    res = @target.view_layouts_base_body_bottom(@context)
    assert_equal false, res.blank?   
  end
  
  test "should hidden feecback_tab when hidden_anonymous" do
    Setting.plugin_redmine_site_feedback['hide_anonymous'] = "true"
    assert_equal true, SiteFeedback.hide_anonymous?  
  end
end

