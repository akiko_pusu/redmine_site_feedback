= Redmine Site Feedback Tab plugin

Plugin to provide site-wide "feedback tab", that allows users to access feedback forums, issue tracker & information right away.
Please note this plugin does not have any functions such as feedback form, sending email.   

=== Plugin installation

1. Copy the plugin directory into the plugins/ directory.
2. Do migration task. (e.g. rake redmine:plugins:migrate RAILS_ENV=production)
3. (Re)Start Redmine.

= Note

This plugin using jQuery. Please remove this plugin if you find something wrong on Redmine's Javascript action.


